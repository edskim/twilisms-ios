//
//  AddressBookHandler.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/19/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "AddressBookHandler.h"

@interface AddressBookHandler () {
    __strong NSMutableDictionary *allContacts;      //stores full_name:(labels:numbers)
    __strong NSMutableDictionary *fullNameByStrippedNumber;
}

@end

@implementation AddressBookHandler

+ (AddressBookHandler *)sharedHandler
{
    static AddressBookHandler *sharedHandler = nil;
    if (!sharedHandler) {
        sharedHandler = [[super allocWithZone:nil] init];
    }
    
    return sharedHandler;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedHandler];
}

- (id)init
{
    self = [super init];
    if (self) {
        allContacts = [NSMutableDictionary new];
        fullNameByStrippedNumber = [NSMutableDictionary new];
        [self populateContactsFromAddressBook];
    }
    
    return self;
}

- (NSDictionary *)getContactsMatchingString:(NSString *)searchTerm
{
    if (!searchTerm || [searchTerm length] == 0) {
        return [NSDictionary dictionaryWithDictionary:allContacts];
    }
    
    //store all contacts containing search string
    NSMutableDictionary *matchingContacts = [NSMutableDictionary new];
    for (NSString *fullName in allContacts.allKeys) {
        if ([fullName rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location != NSNotFound){
            [matchingContacts setObject:[allContacts objectForKey:fullName] forKey:fullName];
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:matchingContacts];
}

- (NSString *)getFullNameForNumber:(NSString *)number
{
    return [fullNameByStrippedNumber objectForKey:[self numberToIntlFormat:number]];
}

- (NSString *)numberToIntlFormat:(NSString *)number
{
    NSMutableString *numberMut = [NSMutableString stringWithString:number];
    NSRegularExpression *nonDigitRegex = [NSRegularExpression regularExpressionWithPattern:@"[^0-9+]" options:0 error:nil];
    [nonDigitRegex replaceMatchesInString:numberMut options:0 range:NSMakeRange(0, [numberMut length]) withTemplate:@""];
    
    //Assume US format (ugly i know)
    if (![numberMut hasPrefix:@"+"]) {
        if ([numberMut hasPrefix:@"1"]) {
            [numberMut insertString:@"+" atIndex:0];
        } else {
            [numberMut insertString:@"+1" atIndex:0];
        }
    }
    
    return [NSString stringWithString:numberMut];
}

#pragma mark Helper methods

- (void)populateContactsFromAddressBook
{
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (granted) {
            CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
            CFIndex numPeople = ABAddressBookGetPersonCount(addressBook);
            
            for (int i = 0; i < numPeople; ++i) {
                ABRecordRef person = CFArrayGetValueAtIndex(allPeople, i);
                
                NSMutableString *fullName = [NSMutableString new];
                NSString *firstName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);
                NSString *lastName = (__bridge NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);
                if (firstName)
                    [fullName appendString:firstName];
                if (lastName)
                    [fullName appendFormat:@" %@", lastName];
                [allContacts setObject:[NSMutableDictionary new] forKey:fullName];
                
                ABMultiValueRef phoneNumbers = ABRecordCopyValue(person, kABPersonPhoneProperty);
                CFIndex numNumbers = ABMultiValueGetCount(phoneNumbers);
                for (int j = 0; j < numNumbers; ++j) {
                    CFStringRef numberTypeCFS = ABMultiValueCopyLabelAtIndex(phoneNumbers, j);
                    NSString *numberType = (__bridge NSString *)ABAddressBookCopyLocalizedLabel(numberTypeCFS);
                    NSString *number = (__bridge NSString *)ABMultiValueCopyValueAtIndex(phoneNumbers, j);
                    [[allContacts objectForKey:fullName] setObject:number forKey:numberType];
                    [fullNameByStrippedNumber setObject:fullName forKey:[self numberToIntlFormat:number]];
                }
            }
        }
    });
}

@end
