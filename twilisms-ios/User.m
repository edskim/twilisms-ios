//
//  User.m
//  twilisms-ios
//
//  Created by Edward Kim on 1/9/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import "User.h"
#import "Chat.h"


@implementation User

@dynamic email;
@dynamic chats;

@end
