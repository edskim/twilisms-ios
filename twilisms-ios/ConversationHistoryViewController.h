//
//  ConversationHistoryViewController.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/10/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConversationHistoryViewController : UITableViewController

@end
