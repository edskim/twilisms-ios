//
//  ConversationHistoryViewController.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/10/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "Chat.h"
#import "ChatViewController.h"
#import "ConversationHistoryViewController.h"
#import "ItemStore.h"
#import "MBProgressHUD.h"
#import "Message.h"
#import "NewChatViewController.h"
#import "ServerHandler.h"
#import "UserAccount.h"

@interface ConversationHistoryViewController ()

@end

@implementation ConversationHistoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textNotificationReceived:) name:@"msg_received" object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Twilisms";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(newChatButtonPressed)];
    
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStyleBordered target:[UserAccount sharedHandler] action:@selector(logout)];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Chats" style:UIBarButtonItemStyleBordered target:nil action:nil];
    
    [[UserAccount sharedHandler] loginWithEmail:nil withPassword:nil withCompletionBlock:^(BOOL successful) {}];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[ItemStore sharedStore] allChats] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ChatCell"];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
        cell.textLabel.shadowOffset = CGSizeMake(0, 1);
        cell.textLabel.shadowColor = [UIColor redColor];
    }
    Chat *chat = [[[ItemStore sharedStore] allChats] objectAtIndex:indexPath.row];
    cell.textLabel.text =   chat.contactName ?
                            [NSString stringWithFormat:@"%@ %@", chat.contactName, chat.contactNumber]:chat.contactNumber;
    cell.detailTextLabel.text = chat.lastMessage.body;
    if ([chat.hasUnreadMsg boolValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Chat *chatToDelete = [[[ItemStore sharedStore] allChats] objectAtIndex:indexPath.row];
        [[ItemStore sharedStore] removeChat:chatToDelete];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Chat *chat = [[[ItemStore sharedStore] allChats] objectAtIndex:indexPath.row];
    ChatViewController *chatViewController = [[ChatViewController alloc] initWithChat:chat];
    [self.navigationController pushViewController:chatViewController animated:YES];
}

#pragma mark - Heper Methods

-(void)chatViewControllerDidReturnChat:(Chat *)chat
{
    BOOL numberIsEmpty = chat ? NO:YES;
    [self dismissViewControllerAnimated:numberIsEmpty completion:^{
        if (!numberIsEmpty) {
            ChatViewController *chatViewController = [[ChatViewController alloc] initWithChat:chat];
            [self.navigationController pushViewController:chatViewController animated:YES];
            [self.tableView reloadData];
        }
    }];
}

- (void)newChatButtonPressed
{
    NewChatViewController *newMsgController = [[NewChatViewController alloc] init];
    newMsgController.modalPresentationStyle = UIModalPresentationFullScreen;
    newMsgController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    newMsgController.completionBlock = ^(Chat *chat) {
        [self chatViewControllerDidReturnChat:chat];
    };
    
    [self.navigationController presentViewController:newMsgController animated:YES completion:^{}];
}

- (void)textNotificationReceived:(NSNotification *)notification
{
    NSLog(@"Text notification received in chat history");
    [[ItemStore sharedStore] sortChats];
    [self.tableView reloadData];
}


@end
