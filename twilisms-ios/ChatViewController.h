//
//  ChatViewController.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/10/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "UIBubbleTableViewDataSource.h"
#import <UIKit/UIKit.h>
@class Chat;

extern NSString *applicationURL;

@interface ChatViewController : UIViewController <UIBubbleTableViewDataSource>
@property (strong) Chat *chat;

- (id)initWithChat:(Chat *)chat;
@end
