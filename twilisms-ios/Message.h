//
//  Message.h
//  twilisms-ios
//
//  Created by Edward Kim on 8/19/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chat;

@interface Message : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSDate * dateCreated;
@property (nonatomic, retain) NSString * from;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * to;
@property (nonatomic, retain) NSString * mediaPath;
@property (nonatomic, retain) NSString * mediaType;
@property (nonatomic, retain) NSData * media;
@property (nonatomic, retain) Chat *chat;

@end
