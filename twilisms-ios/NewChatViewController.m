//
//  NewChatViewController.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/10/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "AddressBookHandler.h"
#import "Chat.h"
#import "ItemStore.h"
#import "NewChatViewController.h"

@interface NewChatViewController () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate> {
    __strong UITableView *tableView;
    __strong NSDictionary *matchingContacts; //stores full_name:(labels:numbers) for filtered results
    
}
@property (weak, nonatomic) IBOutlet UITextField *toTextField;

@end

@implementation NewChatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        matchingContacts = [NSDictionary new];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    self.toTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    self.toTextField.returnKeyType = UIReturnKeyDone;
    self.toTextField.delegate = self;
    [self.toTextField becomeFirstResponder];
    
    [self.toTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITextField delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    Chat *chat =    [self.toTextField.text length] > 0 ?
                    [[ItemStore sharedStore] createChatForContactName:nil withNumber:self.toTextField.text]:nil;
    self.completionBlock(chat);
    return YES;
}

#pragma mark UITableView data source methods

- (UITableViewCell *)tableView:(UITableView *)tblView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tblView dequeueReusableCellWithIdentifier:@"ContactCell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ContactCell"];
        cell.detailTextLabel.textColor = [UIColor whiteColor];
        cell.detailTextLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:18.0];
        cell.textLabel.shadowOffset = CGSizeMake(0, 1);
        cell.textLabel.shadowColor = [UIColor redColor];
    }
    NSDictionary *numbersByLabel = [matchingContacts.allValues objectAtIndex:indexPath.section];
    cell.textLabel.text =  [numbersByLabel.allValues objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = [numbersByLabel.allKeys objectAtIndex:indexPath.row];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[matchingContacts.allValues objectAtIndex:section] count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [matchingContacts.allKeys count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [matchingContacts.allKeys objectAtIndex:section];
}

#pragma mark UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [matchingContacts.allKeys objectAtIndex:indexPath.section];
    NSString *number = [[[matchingContacts objectForKey:name] allValues] objectAtIndex:indexPath.row];
    Chat *chat = [[ItemStore sharedStore] createChatForContactName:name withNumber:number];
    self.completionBlock(chat);
}

#pragma mark Helper methods

- (void)keyBoardDidShow:(NSNotification *)notification
{
    NSDictionary *keyboardInfo = [notification userInfo];
    NSValue *keyboardFrameEnd = [keyboardInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameEndRect = [keyboardFrameEnd CGRectValue];
    
    //setup tableview frame
    CGFloat tableViewOriginX = 0.0;
    CGFloat tableViewOriginY =  self.toTextField.frame.origin.y + self.toTextField.frame.size.height + 1.0;
    CGFloat tableViewWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat tableViewHeight =   [UIScreen mainScreen].bounds.size.height -
                                [UIApplication sharedApplication].statusBarFrame.size.height -
                                self.toTextField.frame.size.height -
                                keyboardFrameEndRect.size.height;
    CGRect tableViewRect =  CGRectMake(tableViewOriginX, tableViewOriginY, tableViewWidth, tableViewHeight);
    tableView = [[UITableView alloc] initWithFrame:tableViewRect];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if ([textField.text length] > 0) {
        if (tableView.superview == nil) {
            [self.view addSubview:tableView];
        }
        
        matchingContacts = [[AddressBookHandler sharedHandler] getContactsMatchingString:textField.text];
        [tableView reloadData];
    } else if (tableView.superview != nil) {
        [tableView removeFromSuperview];
    }
}

@end
