//
//  AppDelegate.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/8/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
