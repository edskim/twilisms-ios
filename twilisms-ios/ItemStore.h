//
//  ItemStore.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/12/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Chat;
@class Message;

@interface ItemStore : NSObject

+ (ItemStore *)sharedStore;
- (NSArray *)allChats;                                      //return all chats in order of descending date
- (void)sortChats;

- (Chat *)createChatForContactName:(NSString *)name withNumber:(NSString *)number;
- (void)removeChat:(Chat *)chat;                            //remove chat and all messages
- (Message *)createMessageForChat:(Chat *)chat;
- (BOOL)saveChanges;
- (void)loadAllChats;

- (NSString *)itemArchivePath;

@end
