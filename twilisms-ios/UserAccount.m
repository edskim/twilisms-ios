//
//  UserAccount.m
//  twilisms-ios
//
//  Created by Edward Kim on 1/8/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

static const UInt8 kKeychainItemIdentifier[]    = "com.twilio.twilisms\0";

#import <Security/Security.h>
#import "ItemStore.h"
#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "ServerHandler.h"
#import "UserAccount.h"

@interface UserAccount () {
    __strong NSMutableDictionary *genericPasswordQuery;
    __strong NSMutableDictionary *keychainData;
    __weak LoginViewController *loginViewController;
}

- (NSMutableDictionary *)dictionaryToSecItemFormat:(NSDictionary *)dictionaryToConvert;
- (NSMutableDictionary *)secItemFormatToDictionary:(NSDictionary *)dictionaryToConvert;
- (void)writeToKeychain;
- (void)resetKeychainItem;

@end

@implementation UserAccount
@synthesize numbers, deviceToken;
@synthesize email = _email;
@synthesize password = _password;

+ (UserAccount *)sharedHandler
{
    static UserAccount *sharedHandler = nil;
    if (!sharedHandler) {
        sharedHandler = [[super allocWithZone:nil] init];
    }
    
    return sharedHandler;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedHandler];
}

- (id)init
{
    self = [super init];
    if (self) {
        self.numbers = @[];
        
        OSStatus keychainErr = noErr;

        genericPasswordQuery = [[NSMutableDictionary alloc] init];
        [genericPasswordQuery setObject:(__bridge id)kSecClassGenericPassword
                                 forKey:(__bridge id)kSecClass];
        
        NSData *keychainItemID = [NSData dataWithBytes:kKeychainItemIdentifier
                                                length:strlen((const char *)kKeychainItemIdentifier)];
        [genericPasswordQuery setObject:keychainItemID forKey:(__bridge id)kSecAttrGeneric];
        
        [genericPasswordQuery setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
        [genericPasswordQuery setObject:(id)kCFBooleanTrue
                                 forKey:(__bridge id)kSecReturnAttributes];
        
        CFDictionaryRef outDictionaryCF = nil;
        keychainErr = SecItemCopyMatching((__bridge CFDictionaryRef)genericPasswordQuery,
                                          (CFTypeRef *)&outDictionaryCF);
        NSMutableDictionary *outDictionary = (__bridge_transfer NSMutableDictionary *)outDictionaryCF;
        
        if (keychainErr == noErr) {
            // Convert the data dictionary into the format used by the view controller:
            keychainData = [self secItemFormatToDictionary:outDictionary];
            _email = [keychainData objectForKey:(__bridge id)kSecAttrAccount];
            _password = [keychainData objectForKey:(__bridge id)kSecValueData];
        } else if (keychainErr == errSecItemNotFound) {
            [self resetKeychainItem];
        } else {
            // Any other error is unexpected.
            NSAssert(NO, @"Serious error.\n");
        }
    }
    
    return self;
}


- (void)loginWithEmail:(NSString *)email withPassword:(NSString *)password withCompletionBlock:(void (^)(BOOL))completionBlock
{
    if ((email && password) || [self hasValidCredentials]) {
        [[ServerHandler sharedHandler] setDeviceTokenWithEmail:email?email:_email withPassword:password?password:_password withCompletionBlock:^(BOOL successful) {
            if (successful) {
                if (email && password) {
                    _email = email;
                    _password = password;
                    [keychainData setObject:_email forKey:(__bridge id)kSecAttrAccount];
                    [keychainData setObject:_password forKey:(__bridge id)kSecValueData];
                    [self writeToKeychain];
                }
                
                [[ItemStore sharedStore] loadAllChats];
                [[ServerHandler sharedHandler] retrieveNewMessages];

            } else {
                [self logout];
                loginViewController.errorTextView.text = @"Login failed.";
            }
            completionBlock(successful);
        }];
    } else {
        [self showLoginControllerAnimated:YES];
    }
}

- (void)clearCredentials
{
    self.numbers = @[];
    if ([self hasValidCredentials])
        [self resetKeychainItem];
    _email = nil;
    _password = nil;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [self showLoginControllerAnimated:YES];
}

- (void)logout
{
    UINavigationController *navController = (UINavigationController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIViewController *topViewController = [navController topViewController];
    
    [MBProgressHUD showHUDAddedTo:topViewController.view animated:YES];
    [[ServerHandler sharedHandler] deleteDeviceTokenWithCompletionBlock:^(BOOL successful) {
        if (successful) {
            [self clearCredentials];
        } else {
            
        }
        [MBProgressHUD hideHUDForView:topViewController.view animated:YES];
    }];
}

- (BOOL)hasValidCredentials
{
    return (self.email && self.password);
}

- (void)showLoginControllerAnimated:(BOOL)animated
{
    if (!loginViewController) {
        UINavigationController *navController = (UINavigationController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
        UIViewController *topViewController = [navController topViewController];
        __weak UserAccount *weakSelf = self;
        
        LoginViewController *tempController = [[LoginViewController alloc] init];
        tempController.modalPresentationStyle = UIModalPresentationFullScreen;
        tempController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        tempController.completionBlock = ^(NSString *email, NSString *password){
            [MBProgressHUD showHUDAddedTo:loginViewController.view animated:YES];
            [weakSelf loginWithEmail:email withPassword:password withCompletionBlock:^(BOOL successful) {
                [MBProgressHUD hideHUDForView:loginViewController.view animated:YES];
                if (successful)
                    [topViewController dismissViewControllerAnimated:YES completion:nil];
            }];
        };
        [topViewController presentViewController:tempController animated:animated completion:nil];
        loginViewController = tempController;
    }
}

#pragma mark Helper Methods

// Implement the secItemFormatToDictionary: method, which takes the attribute dictionary
//  obtained from the keychain item, acquires the password from the keychain, and
//  adds it to the attribute dictionary:
- (NSMutableDictionary *)secItemFormatToDictionary:(NSDictionary *)dictionaryToConvert
{
    // This method must be called with a properly populated dictionary
    // containing all the right key/value pairs for the keychain item.
    
    // Create a return dictionary populated with the attributes:
    NSMutableDictionary *returnDictionary = [NSMutableDictionary
                                             dictionaryWithDictionary:dictionaryToConvert];
    
    // To acquire the password data from the keychain item,
    // first add the search key and class attribute required to obtain the password:
    [returnDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    [returnDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    // Then call Keychain Services to get the password:
    CFDataRef passwordDataCF = NULL;
    OSStatus keychainError = noErr; //
    keychainError = SecItemCopyMatching((__bridge CFDictionaryRef)returnDictionary,
                                        (CFTypeRef *)&passwordDataCF);
    NSData *passwordData = (__bridge_transfer NSData *)passwordDataCF;
    if (keychainError == noErr)
    {
        // Remove the kSecReturnData key; we don't need it anymore:
        [returnDictionary removeObjectForKey:(__bridge id)kSecReturnData];
        
        // Convert the password to an NSString and add it to the return dictionary:
        NSString *passwordKC = [[NSString alloc] initWithBytes:[passwordData bytes]
                                                       length:[passwordData length] encoding:NSUTF8StringEncoding];
        [returnDictionary setObject:passwordKC forKey:(__bridge id)kSecValueData];
    }
    // Don't do anything if nothing is found.
    else if (keychainError == errSecItemNotFound) {
        NSAssert(NO, @"Nothing was found in the keychain.\n");
    }
    // Any other error is unexpected.
    else
    {
        NSAssert(NO, @"Serious error.\n");
    }
    
    return returnDictionary;
}

// Implement the dictionaryToSecItemFormat: method, which takes the attributes that
//   you want to add to the keychain item and sets up a dictionary in the format
//  needed by Keychain Services:
- (NSMutableDictionary *)dictionaryToSecItemFormat:(NSDictionary *)dictionaryToConvert
{
    // This method must be called with a properly populated dictionary
    // containing all the right key/value pairs for a keychain item search.
    
    // Create the return dictionary:
    NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionaryToConvert];
    
    // Add the keychain item class and the generic attribute:
    NSData *keychainItemID = [NSData dataWithBytes:kKeychainItemIdentifier
                                            length:strlen((const char *)kKeychainItemIdentifier)];
    [returnDictionary setObject:keychainItemID forKey:(__bridge id)kSecAttrGeneric];
    [returnDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    
    // Convert the password NSString to NSData to fit the API paradigm:
    NSString *passwordString = [dictionaryToConvert objectForKey:(__bridge id)kSecValueData];
    [returnDictionary setObject:[passwordString dataUsingEncoding:NSUTF8StringEncoding]
                         forKey:(__bridge id)kSecValueData];
    return returnDictionary;
}

// Implement the writeToKeychain method, which is called by the mySetObject routine,
//   which in turn is called by the UI when there is new data for the keychain. This
//   method modifies an existing keychain item, or--if the item does not already
//   exist--creates a new keychain item with the new attribute value plus
//  default values for the other attributes.
- (void)writeToKeychain
{
    CFDictionaryRef attributesCF = nil;
    NSMutableDictionary *updateItem = nil;
    
    // If the keychain item already exists, modify it:
    if (SecItemCopyMatching((__bridge CFDictionaryRef)genericPasswordQuery,
                            (CFTypeRef *)&attributesCF) == noErr)
    {
        NSDictionary *attributes = (__bridge_transfer NSDictionary *)attributesCF;
        // First, get the attributes returned from the keychain and add them to the
        // dictionary that controls the update:
        updateItem = [NSMutableDictionary dictionaryWithDictionary:attributes];
        
        // Second, get the class value from the generic password query dictionary and
        // add it to the updateItem dictionary:
        [updateItem setObject:[genericPasswordQuery objectForKey:(__bridge id)kSecClass]
                       forKey:(__bridge id)kSecClass];
        
        // Finally, set up the dictionary that contains new values for the attributes:
        NSMutableDictionary *tempCheck = [self dictionaryToSecItemFormat:keychainData];
        //Remove the class--it's not a keychain attribute:
        [tempCheck removeObjectForKey:(__bridge id)kSecClass];
        
        // You can update only a single keychain item at a time.
        NSAssert(SecItemUpdate((__bridge CFDictionaryRef)updateItem,
                               (__bridge CFDictionaryRef)tempCheck) == noErr,
                 @"Couldn't update the Keychain Item." );
    }
    else
    {
        // No previous item found; add the new item.
        // The new value was added to the keychainData dictionary in the mySetObject routine,
        //  and the other values were added to the keychainData dictionary previously.
        
        // No pointer to the newly-added items is needed, so pass NULL for the second parameter:
        NSAssert(SecItemAdd((__bridge CFDictionaryRef)[self dictionaryToSecItemFormat:keychainData],
                            NULL) == noErr, @"Couldn't add the Keychain Item." );
    }
}

- (void)resetKeychainItem
{
    if (!keychainData) //Allocate the keychainData dictionary if it doesn't exist yet.
    {
        keychainData = [[NSMutableDictionary alloc] init];
    }
    else if (keychainData)
    {
        
        CFTypeRef itemListCF = nil;
        NSMutableDictionary *tmpDictionary2 = [genericPasswordQuery mutableCopy];
        NSMutableDictionary *query = nil;
        NSDictionary *item = nil;
        [tmpDictionary2 setObject:(__bridge id)kSecMatchLimitAll forKey:(__bridge id)kSecMatchLimit];
        OSStatus keychainErr = SecItemCopyMatching((__bridge CFDictionaryRef)tmpDictionary2, (CFTypeRef *)&itemListCF);
        
        NSArray *itemList = (__bridge NSArray *)(itemListCF);
        
        if (keychainErr == errSecSuccess) {
			for (item in itemList) {
                query = [(NSDictionary *)item mutableCopy];
				[query setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
				keychainErr = SecItemDelete((__bridge CFDictionaryRef)query);
			}
		}
    }
    
    // Default generic data for Keychain Item:
    [keychainData setObject:@"Item label" forKey:(__bridge id)kSecAttrLabel];
    [keychainData setObject:@"Item description" forKey:(__bridge id)kSecAttrDescription];
    [keychainData setObject:@"" forKey:(__bridge id)kSecAttrAccount];
    [keychainData setObject:@"Service" forKey:(__bridge id)kSecAttrService];
    [keychainData setObject:@"Your comment here." forKey:(__bridge id)kSecAttrComment];
    [keychainData setObject:@"" forKey:(__bridge id)kSecValueData];
}

@end
