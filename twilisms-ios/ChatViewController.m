//
//  ChatViewController.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/10/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "Chat.h"
#import "ChatViewController.h"
#import "ItemStore.h"
#import "Message.h"
#import "NSBubbleData.h"
#import "ServerHandler.h"
#import "UIBubbleTableView.h"
#import "UserAccount.h"


@interface ChatViewController () <UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate> {
    __weak IBOutlet UIBubbleTableView *bubbleTable;
    __weak IBOutlet UIView *textInputView;
    __weak IBOutlet UITextField *textField;
    __strong NSMutableArray *bubbleData;
    __weak IBOutlet UIView *numberView;
    __weak IBOutlet UIButton *numberButton;
    __strong UIActionSheet *numberPickerActionSheet;
    __strong UIPickerView *numberPickerView;
    __strong UIToolbar *numberPickerToolbar;
}
@end

@implementation ChatViewController

- (id)initWithChat:(Chat *)chat
{
    self = [super init];
    if (self) {
        bubbleData = [[NSMutableArray alloc] init];
        self.chat = chat;
        
        //initialize chat bubbles
        for (Message *msg in chat.messages) {
            [bubbleData addObject:[self getBubbleDataForMessage:msg]];
        }
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textNotificationReceived:) name:@"msg_received" object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([bubbleData count] == 0) {
        [[ItemStore sharedStore] removeChat:self.chat];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        bubbleData = [[NSMutableArray alloc] init];
        self.chat = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    bubbleTable.bubbleDataSource = self;
    bubbleTable.showAvatars = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    textField.returnKeyType = UIReturnKeyDone;
    textField.delegate = self;
    
    self.chat.hasUnreadMsg = [NSNumber numberWithBool:NO];
    
    self.navigationItem.title = self.chat.contactName ? self.chat.contactName:self.chat.contactNumber;
    [bubbleTable setContentOffset:CGPointMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    [numberButton setTitle:self.chat.fromNumber forState:UIControlStateNormal];
    
    //setup actionsheet
    CGSize mainViewSize = self.view.frame.size;
    
    CGRect toolbarFrame = CGRectMake(0.0, 0.0, mainViewSize.width, 44.0);
    numberPickerToolbar = [[UIToolbar alloc] initWithFrame:toolbarFrame];
    numberPickerToolbar.tintColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.5];
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelActionSheet:)];
    UIBarButtonItem *doneEditingButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePickingNumber:)];
    UIBarButtonItem * spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [numberPickerToolbar setItems:[NSArray arrayWithObjects:cancelButton,spacer,doneEditingButton, nil]];
    
    CGRect pickerViewFrame = CGRectMake(0.0, toolbarFrame.size.height, mainViewSize.width, mainViewSize.height/3.0);
    numberPickerView = [[UIPickerView alloc] initWithFrame:pickerViewFrame];
    numberPickerView.dataSource = self;
    numberPickerView.delegate = self;
    numberPickerView.showsSelectionIndicator = YES;
    
    numberPickerActionSheet = [[UIActionSheet alloc] init];
    [numberPickerActionSheet addSubview:numberPickerToolbar];
    [numberPickerActionSheet addSubview:numberPickerView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sendMessagePressed:(id)sender
{
    if (![self.chat.fromNumber isEqualToString:@"None available"] && [textField.text length] > 0) {
        //Add message to CoreData
        Message *newMessage = [[ItemStore sharedStore] createMessageForChat:self.chat];
        newMessage.to = self.chat.contactNumber;
        newMessage.from = self.chat.fromNumber;
        newMessage.body = textField.text;
        [[ItemStore sharedStore] saveChanges];
        [[ItemStore sharedStore] sortChats];
        
        NSBubbleData *newMsg = [NSBubbleData dataWithText:textField.text date:[NSDate date] type:BubbleTypeMine];
        [bubbleData addObject:newMsg];
        
        [[ServerHandler sharedHandler] sendMessage:newMessage withCompletionBlock:^(BOOL successful) {
            if (!successful) {
                newMsg.view.backgroundColor = [UIColor redColor];
                [bubbleTable reloadData];
            }
        }];
        
        [textField setText:nil];
        [self.view endEditing:YES];
        
        [bubbleTable reloadData];
        [self scrollToLastRow];
    }
}

#pragma mark - UIBubbleTableViewDataSource implementation

- (NSInteger)rowsForBubbleTable:(UIBubbleTableView *)tableView
{
    return [bubbleData count];
}

- (NSBubbleData *)bubbleTableView:(UIBubbleTableView *)tableView dataForRow:(NSInteger)row
{
    return [bubbleData objectAtIndex:row];
}

#pragma mark - Keyboard events

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y -= kbSize.height;
        textInputView.frame = frame;
        
        frame = numberView.frame;
        frame.origin.y -= kbSize.height;
        numberView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height -= kbSize.height + numberView.frame.size.height;
        bubbleTable.frame = frame;
    }];
    
    [numberView setHidden:NO];
    [self scrollToLastRow];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [numberView setHidden:YES];
    
    [UIView animateWithDuration:0.2f animations:^{
        
        CGRect frame = textInputView.frame;
        frame.origin.y += kbSize.height;
        textInputView.frame = frame;
        
        frame = numberView.frame;
        frame.origin.y += kbSize.height;
        numberView.frame = frame;
        
        frame = bubbleTable.frame;
        frame.size.height += kbSize.height + numberView.frame.size.height;
        bubbleTable.frame = frame;
    }];
}

#pragma mark - UITextFieldDelegate implementation

- (BOOL)textFieldShouldReturn:(UITextField *)txtField {
    [txtField resignFirstResponder];
    return YES;
}

#pragma mark PickerView data source methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [[[UserAccount sharedHandler] numbers] count];
}

#pragma mark PickerView delegate methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [[[UserAccount sharedHandler] numbers] objectAtIndex:row];
}

#pragma mark Helper methods

- (void)textNotificationReceived:(NSNotification *)notification
{
    NSLog(@"Text notification received in chat view controller");
    Message *msg = [notification object];
    if (msg.chat == self.chat) {
        [bubbleData addObject:[self getBubbleDataForMessage:msg]];
        
        msg.chat.hasUnreadMsg = [NSNumber numberWithBool:NO];
        [bubbleTable reloadData];
        [self scrollToLastRow];
    }
}

- (NSBubbleData *)getBubbleDataForMessage:(Message *)msg
{
    NSBubbleType bubbleType = [msg.status isEqualToString:@"received"] ? BubbleTypeSomeoneElse:BubbleTypeMine;
    NSBubbleData *bd = [NSBubbleData dataWithText:msg.body date:msg.dateCreated type:bubbleType];
    if (msg.mediaPath && [msg.mediaType hasPrefix:@"image"]) {
        NSData *imageData = [NSData dataWithContentsOfFile:msg.mediaPath];
        if (imageData) {
            UIImage *image = [UIImage imageWithData:imageData];
            bd = [NSBubbleData dataWithImage:image date:msg.dateCreated type:bubbleType];
        }
    }
    if ([msg.status isEqualToString:@"failed"])
        bd.view.backgroundColor = [UIColor redColor];
    return bd;
}

- (void)scrollToLastRow
{
    if ([bubbleTable numberOfSections] > 0) {
        NSInteger lastSec = [bubbleTable numberOfSections] - 1;
        NSInteger lastRow = [bubbleTable numberOfRowsInSection:lastSec] - 1;
        NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:lastRow inSection:lastSec];
        [bubbleTable scrollToRowAtIndexPath:lastIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

- (IBAction)numberButtonPressed:(id)sender {

    CGSize mainViewSize = self.view.frame.size;
    CGFloat actionSheetHeight = numberPickerToolbar.frame.size.height + numberPickerView.frame.size.height;
    CGRect actionSheetFrame = CGRectMake(0.0, mainViewSize.height - actionSheetHeight, mainViewSize.width, actionSheetHeight);
    [numberPickerActionSheet showInView:self.view];
    [numberPickerActionSheet setFrame:actionSheetFrame];
    
    NSArray *numbers = [[UserAccount sharedHandler] numbers];
    [numberPickerView selectRow:0 inComponent:0 animated:NO];
    for (NSInteger i = 0; i < [numbers count]; ++i) {
        if ([[numbers objectAtIndex:i] isEqualToString:self.chat.fromNumber]) {
            [numberPickerView selectRow:i inComponent:0 animated:NO];
            break;
        }
    }
}

- (void)cancelActionSheet:(UIBarButtonItem*)button {
    [numberPickerActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)donePickingNumber:(UIBarButtonItem*)button {
    NSInteger selectedRow = [numberPickerView selectedRowInComponent:0];
    NSArray *numbers = [[UserAccount sharedHandler] numbers];
    if ([numbers count] > selectedRow) {
        self.chat.fromNumber = [numbers objectAtIndex:selectedRow];
        [numberButton setTitle:self.chat.fromNumber forState:UIControlStateNormal];
    }
    [numberPickerActionSheet dismissWithClickedButtonIndex:0 animated:YES];
}

@end
