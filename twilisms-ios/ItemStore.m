//
//  ItemStore.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/12/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "AddressBookHandler.h"
#import "Chat.h"
#import "ItemStore.h"
#import "Message.h"
#import "User.h"
#import "UserAccount.h"

@interface ItemStore () {
    __strong NSMutableArray *allChats;
    __strong NSMutableDictionary *chatsByContactNumber; //key is in international format
    __strong NSManagedObjectContext *context;
    __strong NSManagedObjectModel *model;
}
@property (strong) User *user;

@end

@implementation ItemStore
@synthesize user;

+ (ItemStore *)sharedStore
{
    static ItemStore *sharedStore = nil;
    if (!sharedStore) {
        sharedStore = [[super allocWithZone:nil] init];
    }
    
    return sharedStore;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

- (id)init
{
    self = [super init];
    if (self) {
        
        model = [NSManagedObjectModel mergedModelFromBundles:nil];
        
        NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
        
        NSString *path = [self itemArchivePath];
        NSURL *storeURL = [NSURL fileURLWithPath:path];
        
        NSError *error = nil;
        
        if (![psc addPersistentStoreWithType:NSSQLiteStoreType
                               configuration:nil URL:storeURL options:nil error:&error]) {
            [NSException raise:@"Open failed" format:@"Reason: %@", [error localizedDescription]];
        }
        
        context = [[NSManagedObjectContext alloc] init];
        [context setPersistentStoreCoordinator:psc];
        [context setUndoManager:nil];
        
        [self loadAllChats];
    }
    
    return self;
}

- (NSArray *)allChats
{
    if (!user || ![[UserAccount sharedHandler] email] || ![user.email isEqualToString:[[UserAccount sharedHandler] email]]) {
        [self saveChanges];
        [self loadAllChats];
    }
    return [NSArray arrayWithArray:allChats];
}

- (void)sortChats
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"lastUpdated" ascending:NO];
    NSArray *sortedArray = [allChats sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    allChats = [NSMutableArray arrayWithArray:sortedArray];
}

//Checks for existing chat, if it does not exist then it is created.
//Chats are uniquely identitied solely based on the number.
- (Chat *)createChatForContactName:(NSString *)name withNumber:(NSString *)number
{
    NSString *intlNumber = [[AddressBookHandler sharedHandler] numberToIntlFormat:number];
    if (![chatsByContactNumber objectForKey:intlNumber]) {
        Chat *newChat = [NSEntityDescription insertNewObjectForEntityForName:@"Chat" inManagedObjectContext:context];
        newChat.contactName = name;
        if (name == nil) {
            newChat.contactName = [[AddressBookHandler sharedHandler] getFullNameForNumber:intlNumber];
        }
        newChat.contactNumber = number;
        
        NSArray *numbers = [[UserAccount sharedHandler] numbers];
        newChat.fromNumber = [numbers count] > 0 ? [numbers objectAtIndex:0]:@"None available";
        newChat.user = user;
        [allChats insertObject:newChat atIndex:0];
        [chatsByContactNumber setObject:newChat forKey:intlNumber];
        return newChat;
    }
    return [chatsByContactNumber objectForKey:intlNumber];
}

- (void)removeChat:(Chat *)chat
{
//    for (Message *msg in chat.messages) {
//        [context deleteObject:msg];
//    }
    [context deleteObject:chat];
    [allChats removeObjectIdenticalTo:chat];
    [chatsByContactNumber removeObjectForKey:[[AddressBookHandler sharedHandler] numberToIntlFormat:chat.contactNumber]];
}

- (Message *)createMessageForChat:(Chat *)chat
{
    Message *newMessage = [NSEntityDescription insertNewObjectForEntityForName:@"Message" inManagedObjectContext:context];
    newMessage.chat = chat;             //inverse relationship set automatically
    chat.lastMessage = newMessage;
    chat.lastUpdated = newMessage.dateCreated;
    return newMessage;
}

- (BOOL)saveChanges
{
    NSError *err = nil;
    BOOL successful = [context save:&err];
    if (!successful) {
        NSLog(@"Error saving: %@", [err localizedDescription]);
    }
    
    return successful;
}

- (NSString *)itemArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories objectAtIndex:0];
    
    return [documentDirectory stringByAppendingPathComponent:@"store.data"];
}

- (void)loadAllChats
{
    allChats = [NSMutableArray new];
    chatsByContactNumber = [NSMutableDictionary new];
    
    if ([[UserAccount sharedHandler] hasValidCredentials]) {
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entityDesc = [[model entitiesByName] objectForKey:@"User"];
        [request setEntity:entityDesc];
        
        NSString *email = [[UserAccount sharedHandler] email];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@", email];
        [request setPredicate:predicate];
        
        NSError *error;
        NSArray *result = [context executeFetchRequest:request error:&error];
        
        if (!result) {
            [NSException raise:@"User fetch failed" format:@"Reason: %@", [error localizedDescription]];
        }
        
        if ([result count] == 0) {
            self.user = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:context];
            self.user.email = email;
        } else {
            self.user = [result objectAtIndex:0];
            allChats = [NSMutableArray arrayWithArray:self.user.chats.allObjects];
            [self sortChats];
            
            for (Chat *chat in allChats) {
                [chatsByContactNumber setObject:chat forKey:[[AddressBookHandler sharedHandler] numberToIntlFormat:chat.contactNumber]];
            }
        }
    }
}

@end
