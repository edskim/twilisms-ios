//
//  Chat.m
//  twilisms-ios
//
//  Created by Edward Kim on 1/9/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import "Chat.h"
#import "Message.h"
#import "User.h"


@implementation Chat

@dynamic contactName;
@dynamic contactNumber;
@dynamic hasUnreadMsg;
@dynamic lastUpdated;
@dynamic fromNumber;
@dynamic lastMessage;
@dynamic messages;
@dynamic user;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    [self setLastUpdated:[NSDate date]];
}

@end
