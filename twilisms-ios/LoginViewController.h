//
//  LoginViewController.h
//  twilisms-ios
//
//  Created by Edward Kim on 1/10/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextView *errorTextView;
@property (strong) void (^completionBlock)(NSString *email, NSString *password);
@end
