//
//  User.h
//  twilisms-ios
//
//  Created by Edward Kim on 1/9/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Chat;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString *email;
@property (nonatomic, retain) NSSet *chats;
@end

@interface User (CoreDataGeneratedAccessors)

- (void)addChatsObject:(Chat *)value;
- (void)removeChatsObject:(Chat *)value;
- (void)addChats:(NSSet *)values;
- (void)removeChats:(NSSet *)values;

@end
