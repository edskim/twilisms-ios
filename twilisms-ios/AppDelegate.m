//
//  AppDelegate.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/8/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "AddressBookHandler.h"
#import "AppDelegate.h"
#import "ConversationHistoryViewController.h"
#import "ItemStore.h"
#import "LoginViewController.h"
#import "ServerHandler.h"
#import "UserAccount.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSLog(@"Application launched");
    
    NSDictionary *remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotification) {
        NSLog(@"Remote notification received through didFinishLaunchingWithOptions");
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];

    [AddressBookHandler sharedHandler];
    
    UINavigationController *navController = [UINavigationController new];
    navController.navigationBar.barStyle = UIBarStyleBlack;
    navController.navigationBar.tintColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.5];
    ConversationHistoryViewController *messageHistory = [[ConversationHistoryViewController alloc] init];
    [navController pushViewController:messageHistory animated:NO];
    self.window.rootViewController = navController;

    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert |
                                                                           UIRemoteNotificationTypeBadge |
                                                                           UIRemoteNotificationTypeSound)];
    
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"Entering background");
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"Entering foreground");
    [[ServerHandler sharedHandler] retrieveNewMessages];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark Remote Registration Delegate methods
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSLog(@"%@", deviceToken);
    NSString *deviceTokenString = deviceToken.description;
    [[UserAccount sharedHandler] setDeviceToken:deviceTokenString];
    
    //in case user is logged in before device set
    if ([[UserAccount sharedHandler] hasValidCredentials])
        [[ServerHandler sharedHandler] setDeviceTokenWithCompletionBlock:^(BOOL successful) {}];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error in registration. Error: %@", error);
}

#pragma mark Notification Handlers

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    NSLog(@"Local notification received");
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"Remote notification received");
    if ( application.applicationState == UIApplicationStateActive )
        [[ServerHandler sharedHandler] retrieveNewMessages];
}

#pragma mark Helper methods

- (void)saveContext
{
    if ([[ItemStore sharedStore] saveChanges])
    {
        NSLog(@"Saved all of the coredata items");
    }
    else
    {
        NSLog(@"Could not save any of the coredata items");
    }
}

@end
