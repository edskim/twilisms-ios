//
//  ServerHandler.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/19/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Message;

@interface ServerHandler : NSObject

+ (ServerHandler *)sharedHandler;
- (void)retrieveNewMessages;
- (void)sendMessage:(Message *)msg withCompletionBlock:(void(^)(BOOL successful))completionBlock;
- (void)setDeviceTokenWithCompletionBlock:(void(^)(BOOL successful))completionBlock;
- (void)setDeviceTokenWithEmail:(NSString *)email withPassword:(NSString *)password withCompletionBlock:(void(^)(BOOL successful))completionBlock;
- (void)deleteDeviceTokenWithCompletionBlock:(void(^)(BOOL successful))completionBlock;

@end
