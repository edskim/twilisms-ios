//
//  Message.m
//  twilisms-ios
//
//  Created by Edward Kim on 8/19/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import "Message.h"
#import "Chat.h"


@implementation Message

@dynamic body;
@dynamic dateCreated;
@dynamic from;
@dynamic status;
@dynamic to;
@dynamic mediaPath;
@dynamic mediaType;
@dynamic media;
@dynamic chat;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    [self setDateCreated:[NSDate date]];
}

- (void)prepareForDeletion
{
    if (self.mediaPath) {
        NSLog(@"Attempting to delete file at %@", self.mediaPath);
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:self.mediaPath error:&error];
        if (error) {
            NSLog(@"Deleting file %@ failed:%@", self.mediaPath, error);
        } else {
            NSLog(@"Deleting file %@ suceeded.", self.mediaPath);
        }
    }
}

@end
