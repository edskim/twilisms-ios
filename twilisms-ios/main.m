//
//  main.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/8/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
