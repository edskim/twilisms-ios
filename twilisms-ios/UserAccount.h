//
//  UserAccount.h
//  twilisms-ios
//
//  Created by Edward Kim on 1/8/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAccount : NSObject
@property (strong) NSArray *numbers;
@property (strong, readonly) NSString *email;
@property (strong, readonly) NSString *password;
@property (strong, nonatomic) NSString *deviceToken;

+ (UserAccount *)sharedHandler;
- (BOOL)hasValidCredentials;
- (void)clearCredentials;
- (void)logout;
- (void)showLoginControllerAnimated:(BOOL)animated;
- (void)loginWithEmail:(NSString *)email withPassword:(NSString *)password withCompletionBlock:(void(^)(BOOL successful))completionBlock;

@end
