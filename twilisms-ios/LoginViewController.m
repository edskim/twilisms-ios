//
//  LoginViewController.m
//  twilisms-ios
//
//  Created by Edward Kim on 1/10/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import "LoginViewController.h"
#import "MBProgressHUD.h"
#import "ServerHandler.h"
#import "UserAccount.h"

@interface LoginViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation LoginViewController
@synthesize completionBlock;
@synthesize emailTextField, passwordTextField, errorTextView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.emailTextField.delegate = self;
    self.passwordTextField.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)submitPressed:(id)sender {
    [self.view endEditing:YES];
    
    if ([self.emailTextField.text length]>0 && [self.passwordTextField.text length]>0) {
        
        self.completionBlock(emailTextField.text, passwordTextField.text);
        
    } else {
        if ([self.emailTextField.text length] ==0)
            self.errorTextView.text = @"Please enter username";
        else
            self.errorTextView.text = @"Please enter password";
    }
}


#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self submitPressed:nil];
    return NO;
}

@end
