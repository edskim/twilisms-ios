//
//  ServerHandler.m
//  twilisms-ios
//
//  Created by Edward Kim on 12/19/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import "Chat.h"
#import "ItemStore.h"
#import "Message.h"
#import "RestKit.h"
#import "ServerHandler.h"
#import "UserAccount.h"

NSString *applicationURL = @"https://twilisms.herokuapp.com";

@implementation ServerHandler

+ (ServerHandler *)sharedHandler
{
    static ServerHandler *sharedHandler = nil;
    if (!sharedHandler) {
        sharedHandler = [[super allocWithZone:nil] init];
    }
    
    return sharedHandler;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedHandler];
}

- (id)init
{
    self = [super init];
    if (self) {
        [RKClient clientWithBaseURLString:applicationURL];
    }
    
    return self;
}

- (void)setDeviceTokenWithEmail:(NSString *)email withPassword:(NSString *)password withCompletionBlock:(void (^)(BOOL))completionBlock
{
    UserAccount *account = [UserAccount sharedHandler];
    NSString *deviceToken = account.deviceToken ? account.deviceToken:@"";
    
    RKClient *client = [RKClient sharedClient];
    client.username = email;
    client.password = password;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [client post:@"devices.json" usingBlock:^(RKRequest *request) {
            NSDictionary *params = [NSDictionary dictionaryWithObject:deviceToken forKey:@"device_token"];
            request.params = params;
            request.onDidLoadResponse = ^(RKResponse *response) {
                if ([response isSuccessful]) {
                    NSLog(@"Set device token successful");
                    id parsedResponse = [response parsedBody:nil];
                    NSArray *numbers = [parsedResponse objectForKey:@"numbers"];
                    account.numbers = [NSArray arrayWithArray:numbers];
                } else {
                    NSLog(@"Set device token failed");
                    if ([response isUnauthorized])
                        [[UserAccount sharedHandler] logout];
                }
                
                completionBlock([response isSuccessful]);
            };
            request.onDidFailLoadWithError = ^(NSError *error) {
                completionBlock(NO);
            };
        }];
    });
}

//This is the first step when setting up a user, so this doubles for checking the credentials
- (void)setDeviceTokenWithCompletionBlock:(void (^)(BOOL))completionBlock
{
    UserAccount *account = [UserAccount sharedHandler];
    
    if ([account hasValidCredentials]) {
        [self setDeviceTokenWithEmail:account.email withPassword:account.password withCompletionBlock:completionBlock];
    } else {
        completionBlock(NO);
    }
}

- (void)deleteDeviceTokenWithCompletionBlock:(void (^)(BOOL))completionBlock
{
    UserAccount *account = [UserAccount sharedHandler];
    if ([account hasValidCredentials]) {
        RKClient *client = [RKClient sharedClient];
        client.username = account.email;
        client.password = account.password;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [client delete:@"devices.json" usingBlock:^(RKRequest *request) {
                request.onDidLoadResponse = ^(RKResponse *response) {
                    if ([response isSuccessful]) {
                        NSLog(@"Delete device token successful");
                    } else {
                        NSLog(@"Delete device token failed");
                        if ([response isUnauthorized])
                            [[UserAccount sharedHandler] logout];
                    }
                    completionBlock([response isSuccessful]);
                };
                request.onDidFailLoadWithError = ^(NSError *error) {
                    completionBlock(NO);
                };
            }];
        });
    } else {
        completionBlock(NO);
    }
}

- (void)retrieveNewMessages
{
    UserAccount *account = [UserAccount sharedHandler];
    
    if ([account hasValidCredentials]) {
        RKClient *client = [RKClient sharedClient];
        client.username = account.email;
        client.password = account.password;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [client get:@"messages.json" usingBlock:^(RKRequest *request) {
                request.onDidLoadResponse = ^(RKResponse *response) {
                    if ([response isSuccessful]) {
                        id parsedResponse = [response parsedBody:nil];
                        
                        for (id item in parsedResponse) {
                            NSString *number = [item objectForKey:@"from"];
                            if (number) {
                                //get chat for this contact or create one if it doesn't exist yet
                                Chat *chat = [[ItemStore sharedStore] createChatForContactName:nil withNumber:number];
                                chat.hasUnreadMsg = [NSNumber numberWithBool:YES];
                                
                                Message *newMessage = [[ItemStore sharedStore] createMessageForChat:chat];
                                newMessage.to = [item objectForKey:@"to"];
                                newMessage.from = number;
                                
                                // check if media or just text
                                newMessage.body = [item objectForKey:@"body"];
                                newMessage.mediaType = [item objectForKey:@"content_type"];
                                if (![newMessage.mediaType isEqualToString:@"text"] && [newMessage.mediaType hasPrefix:@"image"]) {
                                    NSLog(@"Getting image from %@", newMessage.body);
                                    NSURL *url = [NSURL URLWithString:newMessage.body];
                                    NSData *mediaData = [NSData dataWithContentsOfURL:url];
                                    
                                    if (mediaData) {
                                        UIImage *image = [[UIImage alloc] initWithData:mediaData];
                                        NSString *directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                        NSString *filename = [url lastPathComponent];
                                        NSString *filepath = nil;
                                        
                                        if ([newMessage.mediaType hasSuffix:@"jpeg"]) {
                                            filepath = [NSString stringWithFormat:@"%@/%@.jpeg", directory, filename];
                                            NSData *fileData = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];
                                            [fileData writeToFile:filepath atomically:YES];
                                            newMessage.mediaPath = filepath;
                                            NSLog(@"Saving image to %@",filepath);
                                        } else if ([newMessage.mediaType hasSuffix:@"png"]) {
                                            filepath = [NSString stringWithFormat:@"%@/%@.png",directory,filename];
                                            NSData *fileData = [NSData dataWithData:UIImagePNGRepresentation(image)];
                                            [fileData writeToFile:filepath atomically:YES];
                                            newMessage.mediaPath = filepath;
                                            NSLog(@"Saving image to %@",filepath);
                                        } else {
                                            NSLog(@"%@ type not recognized", newMessage.mediaType);
                                        }
                                        
                                    }
                                }
                                
                                newMessage.status = @"received";
                                newMessage.dateCreated = [self dateFromISO8601String:[item objectForKey:@"created_at"]];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"msg_received" object:newMessage];
                            }
                        }
                        
                        [[ItemStore sharedStore] saveChanges];
                        
                        NSInteger currBadgeNum = [[UIApplication sharedApplication] applicationIconBadgeNumber];
                        NSInteger newBadgeNum = currBadgeNum - [parsedResponse count];
                        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:newBadgeNum];
                    } else {
                        NSLog(@"Retrieve messages failed");
                        if ([response isUnauthorized]) {
                            [account logout];
                        }
                    }
                };
            }];
        });
    }
}

- (void)sendMessage:(Message *)msg withCompletionBlock:(void (^)(BOOL))completionBlock
{
    UserAccount *account = [UserAccount sharedHandler];
    
    if ([account hasValidCredentials]) {
        RKClient *client = [RKClient sharedClient];
        client.username = account.email;
        client.password = account.password;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [client post:@"messages.json" usingBlock:^(RKRequest *request) {
                NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:  msg.to, @"to",
                                        msg.from , @"from",
                                        msg.body, @"body", nil];
                request.params = [NSDictionary dictionaryWithObject:params forKey:@"message"];
                request.username = account.email;
                request.password = account.password;
                request.authenticationType = RKRequestAuthenticationTypeHTTPBasic;
                request.onDidLoadResponse = ^(RKResponse *response) {
                    if ([response isSuccessful]) {
                        NSLog(@"Sent SMS successfully");
                        msg.status = @"sent";
                    } else {
                        NSLog(@"SMS send failed");
                        msg.status = @"failed";
                        if ([response isUnauthorized])
                            [account logout];
                    }
                    [[ItemStore sharedStore] saveChanges];
                    completionBlock([response isSuccessful]);
                };
                request.onDidFailLoadWithError = ^(NSError *error) {
                    msg.status = @"failed";
                    completionBlock(NO);
                };
            }];
        });
    }
}

#pragma mark Helper methods

- (NSDate *)dateFromISO8601String:(NSString *)dateString
{
    if (!dateString) return nil;
    if ([dateString hasSuffix:@"Z"]) {
        dateString = [[dateString substringToIndex:(dateString.length-1)] stringByAppendingString:@"-0000"];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:locale];
    
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

@end
