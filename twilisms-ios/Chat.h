//
//  Chat.h
//  twilisms-ios
//
//  Created by Edward Kim on 1/9/13.
//  Copyright (c) 2013 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Message, User;

@interface Chat : NSManagedObject

@property (nonatomic, retain) NSString * contactName;
@property (nonatomic, retain) NSString * contactNumber;
@property (nonatomic, retain) NSNumber * hasUnreadMsg;
@property (nonatomic, retain) NSDate * lastUpdated;
@property (nonatomic, retain) NSString * fromNumber;
@property (nonatomic, retain) Message *lastMessage;
@property (nonatomic, retain) NSOrderedSet *messages;
@property (nonatomic, retain) User *user;
@end

@interface Chat (CoreDataGeneratedAccessors)

- (void)insertObject:(Message *)value inMessagesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromMessagesAtIndex:(NSUInteger)idx;
- (void)insertMessages:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeMessagesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInMessagesAtIndex:(NSUInteger)idx withObject:(Message *)value;
- (void)replaceMessagesAtIndexes:(NSIndexSet *)indexes withMessages:(NSArray *)values;
- (void)addMessagesObject:(Message *)value;
- (void)removeMessagesObject:(Message *)value;
- (void)addMessages:(NSOrderedSet *)values;
- (void)removeMessages:(NSOrderedSet *)values;
@end
