//
//  AddressBookHandler.h
//  twilisms-ios
//
//  Created by Edward Kim on 12/19/12.
//  Copyright (c) 2012 Twilio, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddressBookHandler : NSObject

+ (AddressBookHandler *)sharedHandler;

- (NSDictionary *)getContactsMatchingString:(NSString *)searchTerm;
- (NSString *)getFullNameForNumber:(NSString *)number;
- (NSString *)numberToIntlFormat:(NSString *)number;

@end
